#include <stdint.h>
#include <util.h>

#include "encoding.h"

#define NB_CORES 2

// instantiation macros for atomic memory operation (non LR/SC)
// see also
// https://github.com/torvalds/linux/blob/ef78e5ec9214376c5cb989f5da70b02d0c117b66/arch/riscv/include/asm/atomic.h#L94
#define ATOMIC_FETCH_OP(ret, mem, i, asm_op, asm_type) \
  __asm__ __volatile__ (                               \
    " amo" #asm_op "." #asm_type " %1, %2, %0"         \
    : "+A" (mem), "=r" (ret)                           \
    : "r" (i)                                          \
    : "memory");

#define ATOMIC_OP(mem, i, asm_op, asm_type)      \
  __asm__ __volatile__ (                         \
    " amo" #asm_op "." #asm_type " zero, %1, %0" \
    : "+A" (mem)                                 \
    : "r" (i)                                    \
    : "memory");

#define LR_OP(ret, mem, asm_type)               \
  __asm__ __volatile__ (                        \
    " lr." #asm_type " %1, %0"                  \
    : "+A" (mem), "=r" (ret)                    \
    :                                           \
    : "memory");

#define SC_OP(ret, mem, i, asm_type)            \
  __asm__ __volatile__ (                        \
    " sc." #asm_type " %1, %2, %0"              \
    : "+A" (mem), "=r" (ret)                    \
    : "r" (i)                                   \
    : "memory");


typedef struct {
    int counter;
} atomic_t;

static inline int atomic_xchg(atomic_t *v, int n)
{
    register int c;

    __asm__ __volatile__ (
            "amoswap.w.aqrl %0, %2, %1"
            : "=r" (c), "+A" (v->counter)
            : "r" (n));
    return c;
}

static inline void mb(void)
{
    // __asm__ __volatile__ ("fence");
}

void get_lock(atomic_t *lock)
{
    while (atomic_xchg(lock, 1) == 1)
        ;
    mb();
}

void put_lock(atomic_t *lock)
{
    mb();
    atomic_xchg(lock, 0);
}

__attribute__ ((aligned (64))) volatile int shared_cpt = 0; // Must be aligned 64 for smem cache line size
__attribute__ ((aligned (64))) static atomic_t buf_lock = { .counter = 0 };
__attribute__ ((aligned (64))) static volatile uint32_t mbarrier = 0;

int main() {
    uint32_t hartid = read_csr(mhartid);
    uint32_t local_cpt = 0;
    int exp_cpt = hartid;
    for(int i = 0; i < 128; i++) { // Lets practice ping pong

        while(shared_cpt != exp_cpt) { ; } // Wait turn
        exp_cpt += NB_CORES;

        // --- Enter CS ---
        // get_lock(&buf_lock);
        // shared_cpt++;
        // // printf("[%d] %d\n", hartid, shared_cpt);
        // put_lock(&buf_lock);
        // --- Leave CS ---

        ATOMIC_OP(shared_cpt, 1, add, w);

        // AMO_ADD
        local_cpt++;
    }

    get_lock(&buf_lock);
    printf("=== [%d] %d / %d ===\n", hartid, local_cpt, shared_cpt);
    put_lock(&buf_lock);

    ATOMIC_OP(mbarrier, 1, add, w);
    while(mbarrier != NB_CORES) { /* Wait */ }

    return 0;
}