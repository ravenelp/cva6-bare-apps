#include <util.h>


int pow(int a, int b){
    int ret = a;
    while(b--){
        ret *= a;
    }
    return ret;
}

int check_prime(int a)
{
   int c;
 
   for ( c = 2 ; c <= a - 1 ; c++ )
   { 
      if ( a%c == 0 )
     return 0;
   }
   return 1;
}

int fibonacci(int n)
{
  if (n == 0 || n == 1)
    return n;
  else
    return (fibonacci(n-1) + fibonacci(n-2));
}



int abs(int x){
    return x > 0 ? x : -x;
}

int sqRoot(int number) {
   int x = number, y = 1;              //initial guess as number and 1
   int precision = 1;           //the result is correct upto 0.000001

   while(abs(x - y)/abs(x) > 1) {
      x = (x + y)/2;
      y = number/x;
   }
   return x;
}



volatile int y = 45;
volatile int g1 = 123;

int main(void){
    for(int i = 0; i < 100; i++){
        volatile int x = pow(y, y);
    }

    // for(int i = 0; i < 20; i++){
    //    volatile int x2 = fibonacci(i);
    // }

    // for(int i = 0; i < 1000; i++){
    //     volatile int x3 = check_prime(y);
    // }
    
    // for(int i = 0; i < 100; i++){
    //     volatile int f1 = sqRoot(g1 + i);
    // }

    // volatile int x2 = fibonacci(y/2);
    return 0;
}

