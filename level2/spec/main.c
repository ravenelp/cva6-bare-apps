#include <stdint.h>

#define HADAMARD4(d0, d1, d2, d3, s0, s1, s2, s3) {\
    int t0 = s0 + s1;\
    int t1 = s0 - s1;\
    int t2 = s2 + s3;\
    int t3 = s2 - s3;\
    d0 = t0 + t2;\
    d2 = t0 - t2;\
    d1 = t1 + t3;\
    d3 = t1 - t3;\
}
// in: a pseudo-simd number of the form x+(y<<16)
// return: abs(x)+(abs(y)<<16)
static inline uint32_t abs2( uint32_t a )
{
    uint32_t s = ((a>>15)&0x10001)*0xffff;
    return (a+s)^s;
}

volatile int x264_pixel_satd_8x4( uint8_t *pix1, int i_pix1, uint8_t *pix2, int i_pix2 )
{
    uint32_t tmp[4][4];
    uint32_t a0, a1, a2, a3;
    int sum = 0;
    for( int i = 0; i < 4; i++, pix1 += i_pix1, pix2 += i_pix2 )
    {
        a0 = (pix1[0] - pix2[0]) + ((pix1[4] - pix2[4]) << 16);
        a1 = (pix1[1] - pix2[1]) + ((pix1[5] - pix2[5]) << 16);
        a2 = (pix1[2] - pix2[2]) + ((pix1[6] - pix2[6]) << 16);
        a3 = (pix1[3] - pix2[3]) + ((pix1[7] - pix2[7]) << 16);
        HADAMARD4( tmp[i][0], tmp[i][1], tmp[i][2], tmp[i][3], a0,a1,a2,a3 );
    }
    for( int i = 0; i < 4; i++ )
    {
        HADAMARD4( a0, a1, a2, a3, tmp[0][i], tmp[1][i], tmp[2][i], tmp[3][i] );
        sum += abs2(a0) + abs2(a1) + abs2(a2) + abs2(a3);
    }
    return (((uint16_t)sum) + ((uint32_t)sum>>16)) >> 1;
}

/*
* pixel_satd_WxH: sum of 4x4 Hadamard transformed differences
*/
int mine(uint8_t *pix1, int i_pix1, uint8_t *pix2, int i_pix2 ){
    int a0, a1, a2, a3;
    for( int i = 0; i < 4; i++, pix1 += i_pix1, pix2 += i_pix2 )    {
        a0 += (pix1[0] - pix2[0]) + ((pix1[4] - pix2[4]));
        a1 += (pix1[1] - pix2[1]) + ((pix1[5] - pix2[5]));
        a2 += (pix1[2] - pix2[2]) + ((pix1[6] - pix2[6]));
        a3 += (pix1[3] - pix2[3]) + ((pix1[7] - pix2[7]));
    }
    return a0 + a1 + a2 + a3;
}

#include <stddef.h>

volatile uint8_t tab1[64];
volatile uint8_t tab2[64];
volatile uint8_t tab3[64];
volatile uint8_t tab4[64];
volatile uint8_t tab5[64];
volatile uint8_t tab6[64];


int main(){
    volatile int y = 8;
    volatile int z = 1024;
    volatile int x;
    x = mine(tab1, y, tab2, y);
    x = mine(tab3, y, tab4, y);
    x = mine(tab5, y, tab6, y);
    x = mine(tab1, y, tab2, y);
    x = mine(tab3, z, tab4, z);
    x = mine(tab5, z, tab6, z);

    return 0;
}