#include "util.h"

int poly4(int, int*);

int weights[4] = {0x42, 0x43, 0x44, 0x45};

int main(void) {
    int res = 0;
    for (int i = 0; i < 100; i++) {
        res += poly4(i % 10, weights);
    }
    // printf("res = %d\n", res);
}
