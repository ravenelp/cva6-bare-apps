
#include <stdint.h>

// float tab[8] = {0.1, 0.2, 0.4, 0.5, 0.6, 0.7, 0.8};

volatile double f = 0.1234;

int main(void){
    // float res = 0;
    // for(int i = 0; i < 8; i++){
    //     res += tab[i];
    // }

    // Converts a 32-bit signed integer, in integer register rs1 into a double-precision floating-point number in floating-point register rd.
    // fcvt.d.w
    volatile int32_t i = 0xCAFECAFE;
    volatile double f = (double) i;

    // Move the single-precision value in floating-point register rs1 represented in IEEE 754-2008 encoding to the lower 32 bits of integer register rd.
    // fmv.x.w
    // x[rd] = sext(f[rs1][31:0])
    // f += 0.1;
    // double g = f;
    // volatile int32_t i = *((int32_t*)&g);

    return f > 0;
}