/* For copyright information, see olden_v1.0/COPYRIGHT */

#ifndef TORONTO
#include <cm/cmmd.h>
#include <fcntl.h>
#endif

extern int atoi(const char *);

#ifndef TORONTO
extern int __NumNodes;
#else
int NumNodes;
#endif

#ifndef TORONTO
void filestuff()
{
  CMMD_fset_io_mode(stdout, CMMD_independent);
  fcntl(fileno(stdout), F_SETFL, O_APPEND);
  if (CMMD_self_address()) exit(0);
  __InitRegs(0);
}
#endif

int dealwithargs(int argc, char *argv[])
{
  (void)argc;
  (void)argv;
  int level;

#ifndef TORONTO
    __NumNodes = 4;
#else
    NumNodes = 1;
#endif
    level = 6; // DEFAULT: 11

  return level;

}

