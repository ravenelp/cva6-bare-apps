/* For copyright information, see olden_v1.0/COPYRIGHT */

#ifdef NO_PRINTF
#define chatting(...) do {} while(0);
#else
#define chatting printf
#endif
/* =============== NODE STRUCTURE =================== */

struct node { 
  int value;
  struct node *left;
  struct node *right;
};

typedef struct node HANDLE;

typedef struct future_cell_int{
  HANDLE *value;
} future_cell_int;
