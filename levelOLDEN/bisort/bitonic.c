/* For copyright information, see olden_v1.0/COPYRIGHT */

/* =================== PROGRAM bitonic===================== */
/* UP - 0, DOWN - 1 */
#include "node.h"   /* Node Definition */
#include "proc.h"   /* Procedure Types/Nums */
#include <stdio.h>
#include <stdlib.h>

#define CONST_m1 10000
#define CONST_b 31415821
#define RANGE 100

int mrandom(int);

int flag=0,foo=0;

#define LocalNewNode(h,v) \
do{ \
      h = (HANDLE *) malloc(sizeof(HANDLE)); \
      h->value = v; \
	    h->left = NULL; \
	    h->right = NULL; \
	  }while(0);

#define NewNode(h,v,procid) LocalNewNode(h,v)

void InOrder(HANDLE *h) {
  HANDLE *l, *r;
  if ((h != NULL)) {
    l = h->left;
    r = h->right;
    InOrder(l);
    static unsigned char counter = 0;
    if (counter++ == 0)   /* reduce IO */
      chatting("%d @ 0x%x\n",h->value, 0);
    InOrder(r);
  }
}

int mult(int p, int q) {
  int p1, p0, q1, q0;
	
  p1 = p/CONST_m1; p0 = p%CONST_m1;
  q1 = q/CONST_m1; q0 = q%CONST_m1;
  return ((p0*q1+p1*q0) % CONST_m1)*CONST_m1+p0*q0;
}

/* Generate the nth mrandom # */
int skiprand(int seed, int n) {
  for (; n; n--) seed=mrandom(seed);
  return seed;
}

int mrandom(int seed) {
  return mult(seed,CONST_b)+1;
}

HANDLE* RandTree(int n, int seed) {
  HANDLE *h;
  if (n > 1) {
    seed = mrandom(seed);
    NewNode(h,seed % RANGE,0);
    h->left = RandTree((n/2),seed);
    h->right = RandTree((n/2),skiprand(seed,(n)+1));
  } else {
    h = NULL;
  }
  return h;
}

void SwapValue(HANDLE *l, HANDLE *r) {
  int temp,temp2;
  
  temp = l->value;
  temp2 = r->value;
  r->value = temp;
  l->value = temp2;
} 

void
/***********/
SwapValLeft(l,r,ll,rl,lval,rval)
/***********/
HANDLE *l;
HANDLE *r;
HANDLE *ll;
HANDLE *rl;
int lval, rval;
{
  r->value = lval;
  r->left = ll;
  l->left = rl;
  l->value = rval;
} 


void
/************/
SwapValRight(l,r,lr,rr,lval,rval)
/************/
HANDLE *l;
HANDLE *r;
HANDLE *lr;
HANDLE *rr;
int lval, rval;
{  
  r->value = lval;
  r->right = lr;
  l->right = rr;
  l->value = rval;
  /*chatting("Swap Val Right l 0x%x,r 0x%x val: %d %d\n",l,r,lval,rval);*/
} 

int
/********************/
Bimerge(root,spr_val,dir)
/********************/
HANDLE *root;
int spr_val,dir;

{ int rightexchange;
  int elementexchange;
  HANDLE *pl,*pll,*plr;
  HANDLE *pr,*prl,*prr;
  HANDLE *rl;
  HANDLE *rr;
  int rv,lv;


  /*chatting("enter bimerge %x\n", root);*/
  rv = root->value;

  pl = root->left;
  pr = root->right;
  rightexchange = ((rv > spr_val) ^ dir);
  if (rightexchange)
    {
      root->value = spr_val;
      spr_val = rv;
    }
  
  while ((pl != NULL))
    {
      /*chatting("pl = 0x%x,pr = 0x%x\n",pl,pr);*/
      lv = pl->value;        /* <------- 8.2% load penalty */
      pll = pl->left;
      plr = pl->right;       /* <------- 1.35% load penalty */
      rv = pr->value;         /* <------ 57% load penalty */
      prl = pr->left;         /* <------ 7.6% load penalty */
      prr = pr->right;        /* <------ 7.7% load penalty */
      elementexchange = ((lv > rv) ^ dir);
      if (rightexchange)
        if (elementexchange)
          { 
            SwapValRight(pl,pr,plr,prr,lv,rv);
            pl = pll;
            pr = prl;
          }
        else 
          { pl = plr;
            pr = prr;
          }
      else 
        if (elementexchange)
          { 
            SwapValLeft(pl,pr,pll,prl,lv,rv);
            pl = plr;
            pr = prr;
          }
        else 
          { pl = pll;
            pr = prl;
          }
    }
  if ((root->left != NULL))
    { 
      int value;
      rl = root->left;
      rr = root->right;
      value = root->value;

      root->value=Bimerge(rl,value,dir);
      spr_val=Bimerge(rr,spr_val,dir);
    }
  /*chatting("exit bimerge %x\n", root);*/
  return spr_val;
} 

int
/*******************/
Bisort(root,spr_val,dir)
/*******************/
HANDLE *root;
int spr_val,dir;

{ HANDLE *l;
  HANDLE *r;
  int val;
  /*chatting("bisort %x\n", root);*/
  if ((root->left == NULL))  /* <---- 8.7% load penalty */
    { 
     if (((root->value > spr_val) ^ dir))
        {
	  val = spr_val;
	  spr_val = root->value;
	  root->value =val;
	}
    }
  else 
    {
      int ndir;
      l = root->left;
      r = root->right;
      val = root->value;
      /*chatting("root 0x%x, l 0x%x, r 0x%x\n", root,l,r);*/
      root->value=Bisort(l,val,dir);
      ndir = !dir;
      spr_val=Bisort(r,spr_val,ndir);
      spr_val=Bimerge(root,spr_val,dir);
    }
  /*chatting("exit bisort %x\n", root);*/
  return spr_val;
} 

int main(int argc, char **argv) {
  HANDLE *h;
  int sval;
  int n;
  
  n = dealwithargs(argc,argv);
  n = 16;
  chatting("Bisort with %d\n", n);

  h = RandTree(n,12345768);
  sval = mrandom(245867) % RANGE;
  if (flag) {
    InOrder(h);
    chatting("%d\n",sval);
  }
  chatting("**************************************\n");
  chatting("BEGINNING BITONIC SORT ALGORITHM HERE\n");
  chatting("**************************************\n");

  sval=Bisort(h,sval,0);

  if (flag) {
    chatting("Sorted Tree:\n"); 
    InOrder(h);
    chatting("%d\n",sval);
  }

  sval=Bisort(h,sval,1);

  if (flag) {
    chatting("Sorted Tree:\n"); 
    InOrder(h);
    chatting("%d\n",sval);
  }

  return 0;
} 







