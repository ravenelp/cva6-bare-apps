/* For copyright information, see olden_v1.0/COPYRIGHT */

#include <stdlib.h>
#include <stdio.h>

extern int NumNodes,NDim;

extern int flag;


int mylog(int num) {
  int j=0,k=1;
  
  while(k<num) { k*=2; j++; }
  return j;
} 

int dealwithargs(int argc, char *argv[]) {
  (void)argc;
  (void)argv;
  int size;
  flag = 1;
  NumNodes = 1;
  size = 32;
  NDim = mylog(NumNodes);
  return size;
}

