/* For copyright information, see olden_v1.0/COPYRIGHT */

#include "em3d.h"

#ifndef TORONTO
#include <cm/cmmd.h>
#include <fcntl.h>
#endif

#ifdef TORONTO
int NumNodes;
#else
#ifdef OLDEN
extern int __NumNodes;
#endif
#endif

extern int DebugFlag;

#ifndef TORONTO
void filestuff()
{
  CMMD_fset_io_mode(stdout, CMMD_independent);
  fcntl(fileno(stdout), F_SETFL, O_APPEND);
  if (CMMD_self_address()) exit(0);
  __InitRegs(0);
}
#endif

void dealwithargs(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
#ifdef TORONTO
    DebugFlag = 0;
    NumNodes = 1;
#else
#ifdef OLDEN
    DebugFlag = 0;
    __NumNodes = 4;
#endif
#endif
    n_nodes = 64;
    d_nodes = 3;
    local_p = 75;
}


