/* For copyright information, see olden_v1.0/COPYRIGHT */

#pragma once

#include <stdlib.h>
#include "hash.h"
#define MAXPROC 1

#ifdef NO_PRINTF
#define chatting(...) do {} while(0);
#else
#define chatting printf
#endif

extern int NumNodes;

typedef struct vert_st {
  int mindist;
  struct vert_st *next;
  Hash edgehash;
} *Vertex;

typedef struct graph_st {
  Vertex vlist[MAXPROC];
} *Graph;

Graph MakeGraph(int numvert, int numproc);
int dealwithargs(int argc, char *argv[]);

int atoi(const char *);
