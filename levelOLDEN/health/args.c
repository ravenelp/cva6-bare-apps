/* For copyright information, see olden_v1.0/COPYRIGHT */

/*****************************************************************
 * args.c:  Handles arguments to command line.                   *
 *          To be used with health.c.                            *
 *****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "health.h"

void dealwithargs(int argc, char *argv[]) { 
  (void)argc;
  (void)argv;
  max_level = 3;
  max_time = 15;
  seed = 4;
}




