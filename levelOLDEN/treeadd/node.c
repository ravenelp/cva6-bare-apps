/* For copyright information, see olden_v1.0/COPYRIGHT */

/* node.c
 */


#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

extern int runs;

int dealwithargs(int argc, char *argv[]);

typedef struct {
    long 	level;
} startmsg_t;

int main (int argc, char *argv[])
{
    tree_t	*root;
    int level, result = 0;
  
    level = dealwithargs(argc, argv);

    chatting("Treeadd with %d levels with %d runs\n",
	     level, runs);

    /* only processor 0 will continue here. */
    chatting("About to enter TreeAlloc\n");

    root = TreeAlloc (level);

    chatting("About to enter TreeAdd\n");
    {
        int i;
        for (i = 0; i < runs; ++i) {
            result = TreeAdd (0, root);
        }
    }

    chatting("Received result of %d\n",result);
    exit(0);
}

/* TreeAdd:
 */
int TreeAdd (int inc_level, tree_t *t)
{
  if (t == NULL)  {
    return 0;
  }
  else {
    int leftval;
    int rightval;
    tree_t *tleft, *tright;
    int value;

    tleft = t->left;            /* <---- 57% load penalty */
    leftval = TreeAdd(inc_level + 1, tleft);
    tright = t->right;          /* <---- 11.4% load penalty */
    rightval = TreeAdd(inc_level + 1, tright);
    /*chatting("after touch\n");*/
    value = t->val;
    /*chatting("returning from treeadd %d\n",*/
	     /*leftval.value + rightval.value + value);*/
    return leftval + rightval + value;
  }
} /* end of TreeAdd */



