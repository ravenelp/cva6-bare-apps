/* For copyright information, see olden_v1.0/COPYRIGHT */

/* tree.h
 */

#pragma once

#include <stdlib.h>

#ifdef NO_PRINTF
#define chatting(...) do {} while(0);
#else
#define chatting printf
#endif

typedef struct tree {
    int		val;
    struct tree *left, *right;
} tree_t;

extern tree_t *TreeAlloc (int level);
int TreeAdd (int inc_level, tree_t *t);







