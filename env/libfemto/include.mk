# Reference 
ENV_DIR				:= $(ROOT)/env/libfemto
libfemto          	:= $(ENV_DIR)/build/lib/libfemto.a
libfemto_includes 	:= $(ENV_DIR)/build/include
libfemto_lds      	:= $(ENV_DIR)/env/cva6_good_trap_bad_trap/default.lds

default: all

src_dir = .

#--------------------------------------------------------------------
# Sources
#--------------------------------------------------------------------

dirs = $(notdir $(shell find $(src_dir)/* -type d))
bmarks := $(filter-out common build, $(dirs))
$(info $(bmarks))

#--------------------------------------------------------------------
# Build rules
#--------------------------------------------------------------------

XLEN ?= 64
RISCV_PREFIX 	?= riscv$(XLEN)-unknown-elf-
RISCV_GCC 		?= $(RISCV_PREFIX)gcc
RISCV_GCC_OPTS 	?= -DPREALLOCATE=1 -mcmodel=medany -static -std=gnu99 -O2 -fno-math-errno -fno-common -fno-builtin-printf -fno-tree-loop-distribute-patterns $(USER_CFLAGS)
RISCV_LINK 		?= $(RISCV_GCC) -T $(libfemto_lds) $(incs) $(libfemto_includes) $(USER_LDFLAGS)
RISCV_LINK_OPTS ?= -static -nostdlib -nostartfiles -T $(libfemto_lds) $(USER_LDFLAGS) -lm 
RISCV_OBJDUMP 	?= $(RISCV_PREFIX)objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data
RISCV_SIM 		?= spike --isa=rv$(XLEN)gc
RISCV_OBJCOPY 	?= $(RISCV_PREFIX)objcopy

define compile_template
build/$(1).riscv: $(wildcard $(1)/*)
	@mkdir -p $$(@D)
	$$(RISCV_GCC) -I$(1) $$(RISCV_GCC_OPTS) -o $$@ $(wildcard $(1)/*.c) $(libfemto) $$(RISCV_LINK_OPTS)
endef

$(foreach bmark,$(bmarks),$(eval $(call compile_template,$(bmark))))

#------------------------------------------------------------
# Build and run benchmarks on riscv simulator

bmarks_build        := $(addprefix build/,$(bmarks))
bmarks_riscv_bin  	:= $(addsuffix .riscv,      	$(bmarks_build))
bmarks_riscv_dump 	:= $(addsuffix .riscv.dump, 	$(bmarks_build))
bmarks_riscv_out  	:= $(addsuffix .riscv.out,  	$(bmarks_build))

$(info $(bmarks_riscv_bin))

$(bmarks_riscv_dump): %.riscv.dump: %.riscv
	$(RISCV_OBJDUMP) $< > $@

$(bmarks_riscv_out): %.riscv.out: %.riscv
	$(RISCV_SIM) $< > $@

#------------------------------------------------------------
# Default

all: $(bmarks_riscv_dump)
run: $(bmarks_riscv_out)
img: $(bmarks_riscv_img)

#------------------------------------------------------------
# Clean up

clean:
	rm -rf build
