/* --- Copyright Martin Birgmeier 1996. All rights reserved. --------------
 * File:			C.win32/extern/src/rand48.c
 * Purpose:			BSD random number generator
 * Author:			Robert Duncan, May 31 1996
 */

/************************************************************************
 *                                                                      *
 * Copyright (c) 1993 Martin Birgmeier                                  *
 * All rights reserved.                                                 *
 *                                                                      *
 * You may redistribute unmodified or modified versions of this source  *
 * code provided that the above copyright notice and this and the       *
 * following conditions are retained.                                   *
 *                                                                      *
 * This software is provided ``as is'', and comes with no warranties    *
 * of any kind. I shall in no event be liable for anything that happens *
 * to anyone/anything when using this software.                         *
 *                                                                      *
 ************************************************************************/

double
erand48(unsigned short xseed[3]);

double
drand48(void);

long
lrand48(void);

long
nrand48(unsigned short xseed[3]);

long
mrand48(void);

long
jrand48(unsigned short xseed[3]);

void
srand48(long seed);

unsigned short *
seed48(unsigned short xseed[3]);

void
lcong48(unsigned short p[7]);