# cva6-bare-apps

Collection of applications for the cva6.

* `level0` : performance tests of the microarchitecture without setup
* `level1` : 
* `level2` : C applications with printf capability
* `level3` : `Polybench`
* `levelRVT` : `riscv-arch-test`
* `levelRVT` : `riscv-tests`

## Build

```sh
# Build level<LEVEL>/build/*.riscv
make -C level<LEVEL>
```

## Methodology

* The binaries are built in the `level<LEVEL>/build` folder.
* The extension of the riscv bianaries must be `.riscv`.
* Each riscv binary must have the `pass` and `fail` symbols. To get symbole addr use `nm <bin>.riscv | grep ' pass'  | cut -d " " -f1`.
`
