
.section ".text"
.globl _start
_start:

loop: j loop

.globl pass
pass:
li x1, 1
sw x1, tohost, t1
self_loop_pass: j self_loop_pass
.globl fail
fail:
li x1, 11
sw x1, tohost, t1
self_loop_fail:  j self_loop_fail

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0