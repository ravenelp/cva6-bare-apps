
.section ".text"
.globl _start
_start:

# Init
la t1, var
li t4, 1024
add t0, t1, t4

li a0, 0xcafe0000
li a1, 0xcafe1111
li a2, 0xcafe2222
li a3, 0xcafe3333
li a4, 0xcafe4444
li a5, 0xcafe5555
li a6, 0xcafe6666
li a7, 0xcafe7777


la t3, var

# Loop
loop:

addi a0, a0, 1
sw a0, 0(t3)
addi a1, a1, 1
sw a1, 0(t3)
addi a2, a2, 2
sw a2, 0(t3)
addi a3, a3, 3
sw a3, 0(t3)
addi a4, a4, 4
sw a4, 0(t3)
addi a5, a5, 5
sw a5, 0(t3)
addi a6, a6, 6
sw a6, 0(t3)
addi a7, a7, 7
sw a7, 0(t3)

addi  t1, t1, 4
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

var: .dword 4096

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0