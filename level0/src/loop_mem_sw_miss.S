
.section ".text"
.globl _start
_start:

# Init
la t1, var
li t4, 1024*16
add t0, t1, t4

li a2, 0xcafecafe

# Loop
loop:

sw a0, 0*16(t1)
sw a1, 1*16(t1)
sw a2, 2*16(t1)
sw a3, 3*16(t1)
sw a4, 4*16(t1)
sw a5, 5*16(t1)
sw a6, 6*16(t1)
sw a7, 7*16(t1)

addi  t1, t1, 8*16 # 8*cache line size
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

var: .dword 1024*16

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0