
.section ".text"
.globl _start
_start:

# Enable FPU
lui	    a0,      0x2
csrs	mstatus, a0
csrwi	fcsr,    0

# Init
addi  t0, zero, 1000
addi  t1, zero, 0

# fmv.x.w : x[rd] = sext(f[rs1][31:0])
# fmv.w.x   f[rd] =      x[rs1][31:0]

li t2, 0xcafecafecafecafe
fmv.d.x f0, t2
fmv.d.x f1, t2
fmv.d.x f2, t2
fmv.d.x f3, t2
fmv.d.x f4, t2
fmv.d.x f5, t2
fmv.d.x f6, t2
fmv.d.x f7, t2


# Loop
loop:

fcvt.d.w f0, a0 
fcvt.d.w f1, a1 
fcvt.d.w f2, a2 
fcvt.d.w f3, a3 
fcvt.d.w f4, a4 
fcvt.d.w f5, a5 
fcvt.d.w f6, a6 
fcvt.d.w f7, a7 

addi  t1, t1, 1
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0