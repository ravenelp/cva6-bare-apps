
.section ".text"
.globl _start
_start:

# Enable FPU
lui	    a0,      0x2
csrs	mstatus, a0
csrwi	fcsr,    0

# Init
addi  t0, zero, 1000
addi  t1, zero, 0

# fmv.x.w : x[rd] = sext(f[rs1][31:0])
# fmv.w.x   f[rd] =      x[rs1][31:0]


fmv.d.x f8, t2
fmv.d.x f9, t3


li t2, 1
fmv.w.x f0, t2
fmv.w.x f1, t2
fmv.w.x f2, t2
fmv.w.x f3, t2
fmv.w.x f4, t2
fmv.w.x f5, t2
fmv.w.x f6, t2
fmv.w.x f7, t2

fmv.w.x f8, t2

# Loop
loop:

fdiv.s f0, f0, f8
fdiv.s f1, f1, f8
fdiv.s f2, f2, f8
fdiv.s f3, f3, f8
fdiv.s f4, f4, f8 
fdiv.s f5, f5, f8
fdiv.s f6, f6, f8
fdiv.s f7, f7, f8 

addi  t1, t1, 1
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0