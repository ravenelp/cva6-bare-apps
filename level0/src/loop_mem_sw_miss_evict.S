
.section ".text"
.globl _start
_start:


# Init
li a7, 32*1024 # Cacle size bytes

la t1, var
li t4, 1024*32*1024 # 1024 * Cacle size bytes
add t0, t1, t4

li a2, 0xcafecafe

# Loop
loop:

sw a2, 0(t1)

add   t1, t1, a7 # Cacle size
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

var: .dword 1024*32*1024

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0