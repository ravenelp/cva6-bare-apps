
.section ".text"
.globl _start
_start:

# Init
la t1, var
li t4, 1024
add t0, t1, t4

la t3, var

# Loop
loop:

lw a0, 0(t3)
nop
nop
lw a1, 0(t3)
nop
nop
lw a2, 0(t3)
nop
nop
lw a3, 0(t3)
nop
nop
lw a4, 0(t3)
nop
nop
lw a5, 0(t3)
nop
nop
lw a6, 0(t3)
nop
nop
lw a7, 0(t3)
nop
nop
lw a0, 0(t3)
nop
nop
lw a1, 0(t3)
nop
nop
lw a2, 0(t3)
nop
nop
lw a3, 0(t3)
nop
nop
lw a4, 0(t3)
nop
nop
lw a5, 0(t3)
nop
nop
lw a6, 0(t3)
nop
nop
lw a7, 0(t3)
nop
nop

addi  t1, t1, 4
bge   t0, t1, loop

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

var: .dword 4096

.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0