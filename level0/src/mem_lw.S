
.section ".text"
.globl _start
_start:

lw a1, var

#include "goodbadtrap.S"

.section ".tdata.begin"
.globl _tdata_begin
_tdata_begin:

var: .dword 0


.section ".tdata.end"
.globl _tdata_end
_tdata_end:

.section ".tbss.end"
.globl _tbss_end
_tbss_end:

.section ".tohost","aw",@progbits
.align 6
.globl tohost
tohost: .dword 0
.align 6
.globl fromhost
fromhost: .dword 0